#include <iostream>
#include <string>

#include "test.pb.h"

// g++ main.cc test.pb.cc -lprotobuf
int main0()
{
    // 封装了login请求对象的数据
    fixbug::LoginRequest req;
    req.set_name("zhang san");
    req.set_pwd("123456");

    // 对象序列化
    std::string send_str;
    if (req.SerializePartialToString(&send_str))
    {
        std::cout << send_str << std::endl;
    }

    // 反序列化
    fixbug::LoginRequest reqB;
    if (reqB.ParseFromString(send_str))
    {
        std::cout << reqB.name() << std::endl;
        std::cout << reqB.pwd() << std::endl;
    }

    return 0;
}

int main()
{
    // fixbug::LoginResponse rsp;
    // ResultCode * rc = rsp.mutable_result();
    // rc->set_errcode(0);
    // rc->set_errmsg("登录处理失败了");

    fixbug::GetFriendListsResponse rsp;
    fixbug::ResultCode *rc = rsp.mutable_result();
    rc->set_errcode(0);

    fixbug::User *user1 = rsp.add_friend_list();
    user1->set_name("zhang san");
    user1->set_age(20);
    user1->set_sex(fixbug::User::MAN);

    fixbug::User *user2 = rsp.add_friend_list();
    user2->set_name("zhang san");
    user2->set_age(20);
    user2->set_sex(fixbug::User::MAN);

    std::cout << rsp.friend_list_size() << std::endl;
}