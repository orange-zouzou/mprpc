#include <iostream>
#include <string>

#include "user.pb.h" // include_directories(${PROJECT_SOURCE_DIR}/example)
#include "mprpcapplication.h"
#include "rpcprovider.h"
#include "logger.h"

/*
UserService原来是一个本地服务，提供了两个进程内的本地方法，Login和GetFriendLists
*/

class UserService : public fixbug::UserServiceRpc
{
    bool Login(std::string name, std::string pwd)
    {
        std::cout << "doing local service: Login" << std::endl;
        std::cout << "name:" << name << " pwd:" << pwd << std::endl;
        return true;
    }

    // 重写基类UserServiceRpc的虚函数
    void Login(::google::protobuf::RpcController *controller,
               const ::fixbug::LoginRequest *request,
               ::fixbug::LoginResponse *response,
               ::google::protobuf::Closure *done)
    {
        // 框架给业务上报了请求参数LoginRequest，应用获取相应的数据做本地业务
        std::string name = request->name();
        std::string pwd = request->pwd();

        bool login_result = Login(name, pwd); // 做本地业务

        // 把响应写入，包括错误码、错误消息、返回值
        fixbug::ResultCode *code = response->mutable_result();
        code->set_errcode(0);
        code->set_errmsg("");
        response->set_success(login_result);

        // 执行回调操作  执行响应数据的序列化和网络发送，都是由框架完成的
        done->Run();
    }
};

int main(int argc, char **argv)
{
    // 调用框架的初始化操作。服务器地址、端口号不能在代码中写死，写在配置文件中读取，还有日志
    // provider -i config.conf
    MprpcApplication::Init(argc, argv);

    // provider是一个网络服务对象，把UserService对象发布到rpc节点
    RpcProvider provider;
    provider.NotifyService(new UserService());
    // provider.NotifyService(new ProductService());

    // 启动一个rpc服务发布节点，Run()以后，进程进入阻塞状态，等待远程的rpc调用请求
    provider.Run();
    return 0;
}