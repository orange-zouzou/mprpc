#ifndef MPRPC_APPLICATION_H
#define MPRPC_APPLICATION_H

#include "mprpcconfig.h"
// 把下面头文件放进来，让使用者直接包含一个头文件，使用更加方便
#include "mprpcchannel.h"
#include "mprpccontroller.h"

// mprpc框架的初始化类，负责框架的一些初始化操作
class MprpcApplication
{
public:
    static void Init(int argc, char **argv);
    static MprpcApplication &GetInstance();
    static MprpcConfig &GetMprpcConfig();

private:
    static MprpcConfig m_config;

    MprpcApplication() = default;
    MprpcApplication(const MprpcApplication &) = delete;
    MprpcApplication(MprpcApplication &&) = delete;
};

#endif