/usr/bin/ld: //usr/local/lib/libmuduo_net.a(EventLoop.cc.o): relocation R_X86_64_32S against `.LC0' can not be used when making a shared object; recompile with -fPIC
//usr/local/lib/libmuduo_net.a: 无法添加符号: 错误的值
collect2: error: ld returned 1 exit status

库要么在/usrlib/，要么在/usr/local/lib/，发现是有的

原因：编译生成muduo静态库的时候，没有加"-fPIC"，也就是没有产生位置无关代码
导致项目使用它来编译成动态库时，出现了上面的问题

解决：CXX_FLAGS+=-fPIC

set(CXX_FLAGS
...
 -fPIC
 )

 参考博客：
 https://blog.csdn.net/tjcwt2011/article/details/123504742
 https://blog.csdn.net/u010068160/article/details/108261972


 依赖库的时候，库的先后依赖关系有可能会出现链接中符号未定义

 gdb 加入配置文件开始运行
 gdb ./xx
 start -i test.conf

invalid use of incomplete type ‘const class google::protobuf::ServiceDescriptor’
   int methodCnt = pserviceDesc->method_count();
只看到了其前置声明，没有看到定义
#include <google/protobuf/descriptor.h>